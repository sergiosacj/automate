run:
	pipenv run python3 app/main.py $(args)

test:
	pipenv run flake8
	pipenv run pytest
